#!/bin/bash

# Not in use because it is too slow.

w_occupied=$(bspc query -D -d .occupied --names)
w_focused=$(bspc query -D -d focused --names)
w_all=$(bspc query -D --names)

if [[ $w_occupied == *"$w_focused"* ]]; then
	w_focused=""
fi

w_final=$(echo "$w_occupied $w_focused" | tr " " "\n" | sort -n | tr "\n" " ")

bspc monitor -d $w_final
