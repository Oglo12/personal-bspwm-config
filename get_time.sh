#!/bin/bash

the_date=$(date +"%r %a")

raw_time=$(echo "$the_date" | cut -f1 -d " ")

hm_time=${raw_time::-3}

am_pm=$(echo "$the_date" | cut -f2 -d " " | tr '[:upper:]' '[:lower:]')

trimmed_date=$(echo "$hm_time" "$am_pm")

if [[ "${trimmed_date:0:1}" == "0" ]]; then
	echo "${trimmed_date:1:${#trimmed_date}}"
else
	echo "$trimmed_date"
fi
