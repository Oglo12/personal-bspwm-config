#!/bin/bash

source ~/.config/bspwm/settings.sh

s_mode=$1

w_all=$(bspc query -D --names)

w_amount=$(readarray -t abcxyz <<< $(echo $w_all | tr " " "\n"); echo ${#abcxyz[@]})

if [[ $s_mode == "remove" ]]; then
	bspc monitor -d ${w_all:0:$((${#w_all} - 2))}
elif [[ $s_mode == "add" ]]; then
	if [[ $(($w_amount + 1)) -le 10 ]]; then
		w_add_seq="$w_all $(($w_amount + 1))"
		bspc monitor -d $w_add_seq
	fi
fi
