wall_types=("lockscreen.png" "lockscreen.jpg" "lockscreen.jpeg")

directory="$HOME/.config/bspwm"

msg_info () {
	echo -e "\033[0;30m[\033[1;36mInfo\033[0;30m]:\033[0m \033[0;36m$1"
}

success="no"
iteration=0
for i in ${wall_types[@]}
do
	if [[ -f "$directory/$i" ]]; then
		betterlockscreen -u "$directory/$i"
		msg_info "Successfully set lockscreen to '$i'!"
		success="yes"
	else
		if [[ $success == "no" ]]; then
			if [[ $iteration < ${#wall_types} ]]; then
				msg_info "Could not find '$i'! Trying again with next format."
			else
				msg_info "Could not find '$i'! Looks like you have no valid lockscreen image in '$directory/'!"
			fi
		fi
	fi

	iteration=$(($iteration + 1))
done
