#!/bin/bash

msgs[0]="It's not Linux, it's GNU/Linux!"
msgs[1]="C++ is bloated! Use C!"
msgs[2]="Conky is bloat!"
msgs[3]="Why do you need a bar?"
msgs[4]="I only use old Thinkpads."
msgs[5]="I would never bloat my system with another user shell!"
msgs[6]="Why Bspwm? Why not Dwm?"
msgs[7]="Don't use 'sudo', use 'doas'. It is not as bloated!"
msgs[8]="Why do your friends use Windows 11?"
msgs[9]="I'm thinking of using OpenBSD. Linux is just bloated!"
msgs[10]="Why do you use Alacritty? Xterm is less bloated!"
msgs[11]="Anything other than the native packaging format is bloat."
msgs[12]="Ubuntu should burn in a fire."
msgs[13]="You better be on LFS! Anything else if bloat!"

size=${#msgs[@]}
index=$(($RANDOM % $size))

notify-send "Annoying Linux Nerd" "${msgs[$index]}" &
