#!/bin/bash

workspaces=$(bspc query -D --names | tr '\n' ':')
workspaces=${workspaces:0:$((${#workspaces} - 1))}
workspaces=$(echo $workspaces | tr ':' ', ')

echo "[$workspaces]"