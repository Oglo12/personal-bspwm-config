#!/bin/bash

source ~/.config/bspwm/libs.sh

current_info=$(eww-bspwm windows | grep "$1")

if [[ $current_info == "*$1" ]]; then
	exit
fi

eww-bspwm open "$1"
sleep 0.7
eww-bspwm close "$1"
