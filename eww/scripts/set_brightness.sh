#!/bin/bash

new_brightness=$(echo "$1" | sed 's/%//g')

~/.config/bspwm/brightness/change.sh $new_brightness set