#!/bin/bash

source ~/.config/bspwm/brightness/vars.sh

c_mode=$1
c_way=$2

if [[ $c_mode == "normal" ]]; then
	mode=$b_normal
elif [[ $c_mode == "small" ]]; then
	mode=$b_small
else
	mode=$c_mode
fi

if [[ $c_way == "up" ]]; then
	brightnessctl set "$mode"%+
elif [[ $c_way == "down" ]]; then
	brightnessctl set "$mode"%-
elif [[ $c_way == "set" ]]; then
	brightnessctl set "$mode"%
else
	brightnessctl set 100%
fi

brightnessctl --save
