#!/bin/bash

# Gruvbox Dark
gbd_bg="#282828"
gbd_red="#cc241d"
gbd_green="#98971a"
gbd_yellow="#d79921"
gbd_blue="#458588"
gbd_purple="#b16286"
gbd_aqua="#689d6a"
gbd_light_gray="#a89984"
gbd_gray="#928374"
gbd_light_red="#fb4934"
gbd_light_green="#b8bb26"
gbd_light_yellow="#fabd2f"
gbd_light_blue="#83a598"
gbd_light_purple="#d3869b"
gbd_light_aqua="#8ec07c"
gbd_fg="#ebdbb2"
gbd_bg0_h="#1d2021"
gbd_bg0="#282828"
gbd_bg1="#3c3836"
gbd_bg2="#504945"
gbd_bg3="#665c54"
gbd_bg4="#7c6f64"
gbd_orange="#d65d0e"
gbd_bg0_s="#32302f"
gbd_fg4="#a89984"
gbd_fg3="#bdae93"
gbd_fg2="#d5c4a1"
gbd_fg1="#ebdbb2"
gbd_fg0="#fbf1c7"
gbd_light_orange="#fe8019"

# Watashino
wsn_dark_red="#e73e55"
wsn_light_red="#f05971"
wsn_dark_green="#8ee13f"
wsn_light_green="#aefc54"
wsn_dark_yellow="#f3dc42"
wsn_light_yellow="#ffef57"
wsn_dark_blue="#5ca6e6"
wsn_light_blue="#7bc7ed"
wsn_dark_purple="#dd42dc"
wsn_light_purple="#f45bf3"
wsn_dark_aqua="#62cc98"
wsn_light_aqua="#63e9a9"
wsn_dark_orange="#fcad59"
wsn_light_orange="#ffc567"
wsn_gray_0="#131617"
wsn_gray_1="#272a2b"
wsn_gray_2="#3f4343"
wsn_gray_3="#5e6162"
wsn_gray_4="#767a7b"
wsn_gray_5="#96999a"
wsn_gray_6="#aeb1b2"
wsn_gray_7="#c6cacb"
wsn_gray_8="#e5e9ea"
wsn_gray_9="#fdffff"
