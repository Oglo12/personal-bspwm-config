#!/bin/bash

source ~/.config/bspwm/libs.sh
source ~/.config/bspwm/aliases.sh
source ~/.config/rofi/reconf.sh

helper_name="Helper Tool"

notify () {
	notify-send "$helper_name" "$1"
}

openlink () {
	yes="Yes, open link."
	no="No, abort."
	what="What is the link?"

	link=$1

	proceed=$(echo -e "$yes\n$no\n$what" | rofi -i -dmenu -p "Open link?:")

	if [[ $proceed == $yes ]]; then
		xdg-open $link
	elif [[ $proceed == $what ]]; then
		notify "$link"

		openlink $link
	fi
}

confirm () {
	bash ~/.config/rofi/scripts/confirm.sh
}

back="Back"

subpage () {
	echo -e "$2\n$back" | rofi -i -dmenu -p "$1:"
}

des0="Update Rice"
des1="Reset Something"
des2="Reload Picom"
des3="Creator Links"
des4="Tutorials"
des5="Take a Screenshot"
des6="Launch Other Rofi Tool"

desquit="Quit $helper_name"

mainpage () {
	answer=$(echo -e "$des0\n$des1\n$des2\n$des3\n$des4\n$des5\n$des6\n$desquit" | rofi -i -dmenu -p "$helper_name:")

	if [[ $answer == $des0 ]]; then
		opencmd bash ~/.config/bspwm/update-configs.sh --notify
	elif [[ $answer == $des1 ]]; then
		sdes0="Wallpaper"
		sdes1="Lock Screen"

		sanswer=$(subpage "Reset" "$sdes0\n$sdes1")

		if [[ $sanswer == $sdes0 ]]; then
			if [[ $(confirm) == true ]]; then
				opencmd bash ~/.config/bspwm/reset_wallpaper.sh
			fi
		elif [[ $sanswer == $sdes1 ]]; then
			if [[ $(confirm) == true ]]; then
				opencmd bash ~/.config/bspwm/reset_lockscreen.sh
			fi
		fi
	elif [[ $answer == $des2 ]]; then
		killall picom
		~/.config/bspwm/picom/start
		sleep 1.5
	elif [[ $answer == $des3 ]]; then
		youtube="YouTube"
		codeberg="Codeberg (Git Repos)"

		sanswer=$(subpage "Creator Links" "$youtube\n$codeberg")

		if [[ $sanswer == $youtube ]]; then
			openlink "https://youtube.com/@oglothenerd"
		elif [[ $sanswer == $codeberg ]]; then
			openlink "https://codeberg.org/Oglo12"
		fi
	elif [[ $answer == $des4 ]]; then
		sdes0="Configuring SDDM"

		sanswer=$(subpage "Tutorials" "$sdes0")

		if [[ $sanswer == $sdes0 ]]; then
			openlink "https://linuxconfig.org/how-to-customize-the-sddm-display-manager-on-linux"
		fi
	elif [[ $answer == $des5 ]]; then
		sleep 0.5
		flameshot gui
		sleep 0.5
	elif [[ $answer == $des6 ]]; then
		sdes0="Power Menu"
		sdes1="File Manager"

		sanswer=$(subpage "Launch Other" "$sdes0\n$sdes1")

		if [[ $sanswer == $sdes0 ]]; then
			bash ~/.config/rofi/scripts/power_menu.sh
		elif [[ $sanswer == $sdes1 ]]; then
			bash ~/.config/rofi/scripts/fs_manager.sh
		fi
	elif [[ $answer == $desquit ]]; then
		exit
	fi

	mainpage
}

mainpage
