#!/bin/bash

notify-send "Your uptime is..." "$(uptime -p | sed -e 's/up //g')" &
