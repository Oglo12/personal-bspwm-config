#!bin/bash

#### UPDATE THIS!!! ####

bspwm_repo="personal-bspwm-config"
alacritty_repo="personal-alacritty-config-bspwm"
rofi_repo="personal-rofi-config-bspwm"
vis_repo="personal-vis-config"

mkdir ~/.config/
cd ~/.config/

mv bspwm/ bspwm-OLD/
git clone https://codeberg.org/Oglo12/$bspwm_repo.git
mv $bspwm_repo/ bspwm/

mv alacritty/ alacritty-OLD/
git clone https://codeberg.org/Oglo12/$alacritty_repo.git
mv $alacritty_repo/ alacritty/

mv rofi/ rofi-OLD/
git clone https://codeberg.org/Oglo12/$rofi_repo.git
mv $rofi_repo/ rofi/

mv vis/ vis-OLD/
git clone https://codeberg.org/Oglo12/$vis_repo.git
mv $vis_repo/ vis/

cd /home/$USER/

echo "source ~/.config/bspwm/aliases.sh" >> ~/.bashrc

bash ~/.config/bspwm/reset_lockscreen.sh

bash ~/.config/bspwm/reset_wallpaper.sh
