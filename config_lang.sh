#!/bin/bash

source ~/.config/bspwm/settings.sh

mkdir -p $CONFIG

touch $CONFIG/config

config_has_line () {
	success="no"

	for i in $(cat $CONFIG/config); do
		if [[ $i == "#"* ]]; then
			success="no"
		elif [[ $i == $1 ]]; then
			success="yes"
		fi
	done

	echo $success
}
