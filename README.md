# Just My Personal Bspwm Config :-]
> How to Install:
  1. Install all required programs.
  2. (Optional) Configure SDDM theme. [Click for tutorial.](https://linuxconfig.org/how-to-customize-the-sddm-display-manager-on-linux)
  3. Make sure Curl, Bash, and Git are installed.
  4. Run this command: 
  ```
  curl https://codeberg.org/Oglo12/personal-bspwm-config/raw/branch/main/setup.sh | bash
  ```
  5. (Optional) Install Oh My Bash.
  6. Configure GTK and cursor theme in LX Appearance.

> Out of the Box Requirements:
   0. Bspwm - bspwm
   1. Simple X Hotkey Daemon - sxhkd
   2. Rofi - rofi
   3. Picom - picom
   4. Alacritty - alacritty (can be changed in quick settings)
   5. Nerd Fonts Complete - nerd-fonts-complete
   6. Brave - brave (can be changed in the dunst config)
   7. Nitrogen - nitrogen
   8. Lib Notify - libnotify
   9. Notification Daemon - notification-daemon
  10. Dunst - dunst
  11. Better Lock Screen - betterlockscreen
  12. Network Manager - networkmanager
  13. Bluez - bluez
  14. Bluez Utils - bluez-utils
  15. Blueman - blueman
  16. Papirus Icon Theme - papirus-icon-theme
  17. Noto Fonts Emojis - noto-fonts-emoji
  18. Polybar - polybar
  19. LXDE Session Polkit - lxsession
  20. Flameshot - flameshot
  21. Brightness Control - brightnessctl
  22. Getting Lighting Info - light
  23. LX Appearance - lxappearance
  24. YAD - yad
  25. Sox - sox
  26. Eww Widgets - eww-git

> GTK Themes and More to Install (Recommended) (NOTE: The multichoice ones only need you to pick one. But you can use all of them if you want.):
  1. GTK Themes:\
    1. Skeuos GTK Themes - skeuos-gtk\
    2. Mint Themes - mint-themes
  2. Cursor Theme:\
    1. Gruvbox Cursor Themes:
      - Dark Cursor Theme - xcursor-simp1e-gruvbox-dark
      - Light Cursor Theme - xcursor-simp1e-gruvbox-light

> Recommended Programs to Install:
  1. Qt5ct - qt5ct ~ Changing The QT Theme

> Other Optional Programs to Install:
  1. VIS - cli-visualizer ~ See audio waves in the terminal.
  2. TTY Clock - tty-clock-git ~ See the time in the terminal.

> Login Manager (Arch Linux Only):
  1. The Login Manager: SDDM - sddm
  2. Login Manager Theme: Multicolor SDDM Theme - multicolor-sddm-theme

> Other Configurations Used For This Rice:
  1. rofi - https://codeberg.org/Oglo12/personal-rofi-config-bspwm/ - ~/.config/rofi/
  2. alacritty - https://codeberg.org/Oglo12/personal-alacritty-config-bspwm/ - ~/.config/alacritty/

> Other Optional Configurations Used For This Rice:
  1. vis - https://codeberg.org/Oglo12/personal-vis-config/ - ~/.config/vis/

> Optional Extras:
  1. The Wallpapers - https://codeberg.org/Oglo12/just-some-wallpapers/

> How to Update your Version of This Rice:
  1. Open a terminal.
  2. Make sure Git is installed.
  3. Run this command:
     ```
     update-rice
     ```
     If that fails, try this command:
     ```
     sh ~/.config/bspwm/update-configs.sh
     ```
