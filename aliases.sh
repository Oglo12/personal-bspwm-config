source ~/.config/bspwm/settings.sh

alias lock='betterlockscreen -l -q'
alias leave='pkill -KILL -u $USER'
alias update-rice='~/.config/bspwm/update-configs.sh'
alias stop-picom='killall picom'
alias start-picom='~/.config/bspwm/picom/start'
alias refresh-wallpaper='nitrogen --restore'
alias sbu='~/.config/bspwm/brightness/plus.sh'
alias sbd='~/.config/bspwm/brightness/minus.sh'
alias fsm='~/.config/bspwm/rofi/scripts/fs_manager.sh'
alias unlock-welcome='rm ~/.rice_bspwm_welcomed'

workspaces () {
	bspc monitor -d $*
}

reload-picom () {
	stop-picom
	start-picom
}
