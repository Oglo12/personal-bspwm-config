#!/bin/bash

source ~/.config/bspwm/config_lang.sh

if [[ $(config_has_line "no_sound") == "yes" ]]; then
	exit
fi

play "$HOME/.config/bspwm/sounds/$1.ogg" &
