#!/bin/bash

opencmd () {
	alacritty -e $*
}

get_node_specific_info () {
	bspc query -T -n | tr "," "\n" | grep "$1" | sed 's/"//g' | sed "s/$1://g"
}

get_node_tile_info () {
	get_node_specific_info "state"
}

get_node_layer_info () {
	get_node_specific_info "layer"
}

get_node_preselect_info () {
	get_node_specific_info "presel" | sed 's/{splitDir://g'
}

list_connected_monitors () {
	xrandr | grep " connected" | cut -f1 -d " "
}

play_sound () {
	~/.config/bspwm/play_sound.sh $1
}

eww-bspwm () {
	eww --config ~/.config/bspwm/eww/ $*
}
